#include <stdint.h>
typedef int32_t (*add_ptr) (int32_t, int32_t);
int32_t add(int32_t x, int32_t y){   
    return x+y;
}
int32_t addition_operation(add_ptr add, int32_t x, int32_t y){ 
    return add(x, y);
}