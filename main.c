#include <stdio.h>
#include <stdint.h>
#include "add.h"
#include "subtract.h"
#include "multiply.h"
#include "divide.h"
#include "prime.h"

int32_t main(int32_t argc, char **argv){
    int32_t x = 23;
    int32_t y = 3;
    int32_t add_result = add(x, y);
    printf("{%d} + {%d} = {%d}\n", x, y, add_result);
    int32_t subtract_result = subtract(x, y); 
    printf("{%d} - {%d} = {%d}\n",x ,y, subtract_result);
    int32_t multiply_result = multiply(x, y); 
    printf("{%d} * {%d} = {%d}\n",x ,y, multiply_result);
    int32_t divide_result = divide(x, y); 
    printf("{%d} / {%d} = {%d}\n",x ,y, divide_result);
    int32_t modulus_result = modulus(x, y); 
    printf("{%d} %% {%d} = {%d}\n",x ,y, modulus_result);
    int32_t prime_result = prime(x); 
    printf("{%d} is a prime number!\n",prime_result);
    return 0;
}