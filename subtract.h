#include <stdint.h>
typedef int32_t (*subtract_ptr) (int32_t, int32_t);
int32_t subtract(int32_t x, int32_t y){
    return x-y;
}
int32_t subtraction_operation(subtract_ptr subtract, int32_t x, int32_t y){ 
    return subtract(x, y);
}