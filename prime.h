#include <math.h>
#include <stdint.h>
typedef int32_t (*prime_ptr) (int32_t);
int32_t prime(int32_t x){
    int32_t a = 1;
    for (int32_t i=2; i <= (int32_t)sqrt(x); i++){
        if (x % i == 0){
            a = 0;
            break;
        }
    }
    if (a == 1){
        return x;
    }else
    {
        return 0;
    }
    
}
int32_t prime_number_operation(prime_ptr prime, int32_t x){ 
    return prime(x);
}