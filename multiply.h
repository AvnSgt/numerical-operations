#include <stdint.h>
typedef int32_t (*multiply_ptr) (int32_t, int32_t);
int32_t multiply(int32_t x, int32_t y){
    return x*y;
}
int32_t multiplication_operation(multiply_ptr multiply, int32_t x, int32_t y){ 
    return multiply(x, y);
}