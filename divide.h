#include <stdint.h>
#include <stdlib.h>
typedef int32_t (*divide_ptr) (int32_t, int32_t);
int32_t divide(int32_t x, int32_t y){
    if (y==0){
        perror("Divide by Zero");
        exit(1);
    }else
    {
        return x/y;
    }
}
int32_t division_operation(divide_ptr divide, int32_t x, int32_t y){ 
    return divide(x, y);
}
typedef int32_t (*modulus_ptr) (int32_t, int32_t);
int32_t modulus(int32_t x, int32_t y){
    if (y==0){
        perror("Divide by Zero!");
        exit(1);
    }else{
        return x%y;
    }
}
int32_t modulus_operation(modulus_ptr modulus, int32_t x, int32_t y){ 
    return modulus(x, y);
}
